FROM debian:buster-slim AS schemas

RUN apt-get update && apt-get install -y git
WORKDIR /build
RUN git clone https://gitlab.com/gitlab-org/security-products/security-report-schemas.git
COPY scripts/export_security-report-schemas_dist.sh /build/
RUN ./export_security-report-schemas_dist.sh ./security-report-schemas ./dists

FROM docker:20.10

RUN apk add --no-cache ruby ruby-bundler ruby-json ruby-bigdecimal git ruby-dev make gcc musl-dev

COPY --from=schemas /build/dists /usr/share/security-report-schemas

COPY . /build
WORKDIR /build

RUN gem build *.gemspec
RUN gem install ./*.gem
RUN rspec

COPY scripts/secure-*-qa /usr/local/bin/
