require 'secure_analyzer/shared_examples'

describe "report shared examples" do
  context "empty report" do
    before(:context) do
      @report = JSON.parse <<-REPORT
{
  "version": "14.0.0",
  "vulnerabilities": [],
  "remediations": [],
  "scan": {
    "scanner": {
      "id": "gemnasium",
      "name": "Gemnasium",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "2.29.5"
    },
    "type": "dependency_scanning",
    "start_time": "2021-03-26T04:53:16",
    "end_time": "2021-03-26T04:53:17",
    "status": "success"
  }
}
      REPORT
    end

    it_behaves_like "empty report"
  end

  context "valid report" do
    before(:context) do
      @report_path = File.expand_path("../fixtures/gl-actual-dependency-scanning-report.json", __FILE__)
      @report = JSON.parse File.read @report_path

      expected_report_path = File.expand_path("../fixtures/gl-expected-dependency-scanning-report.json", __FILE__)
      @expected_report = JSON.parse File.read expected_report_path
    end

    it_behaves_like "non-empty report", ["yarn.lock"]
    it_behaves_like "recorded report", @expected_report
    it_behaves_like "valid report"
  end
end
