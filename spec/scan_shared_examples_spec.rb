require 'secure_analyzer/shared_examples'

describe "scan shared examples" do
  context "with successful scan" do
    before(:context) do
      @output = "GitLab Gemnasium analyzer v2.29.7"
      @exit_code = 0
      @report_path = __FILE__
    end

    it_behaves_like "successful scan"
  end
end
