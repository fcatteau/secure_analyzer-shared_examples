#!/bin/bash

# Iterate the git tags of the security-report-schemas repository
# and for each tag create a directory named after the tag,
# and that contains the JSON schemas contained in "dist".

set -ex

# first argument: path to the local git clone of the security-report-schemas repository
GIT_CLONE="${1:-"."}"

# second argument: path to where the "dist" directory is exported
TARGET_DIR="${2:-"/usr/share/security-report-schemas"}"

# list tags starting with "v"
TAGS=$(git -C $GIT_CLONE tag -l 'v*')

# iterate tags and export "dist" directory
for tag in $TAGS; do
  tag_dir="$TARGET_DIR/$tag"
  mkdir -p "$tag_dir"
  git -C "$GIT_CLONE" archive --format=tar $tag:dist|tar -C "$tag_dir" -x
done
