require "rspec"
require "json"
require "json-schema"
require_relative "schema_resolver"

RSpec.shared_examples "empty report" do
  describe "version" do
    specify { expect(@report["version"]).to_not be_empty }
  end

  describe "scan" do
    specify { expect(@report["scan"]).to_not be_empty }
  end

  describe "vulnerabilities" do
    specify { expect(@report["vulnerabilities"]).to be_empty }
  end

  describe "dependency_files" do
    specify { expect(@report["dependency_files"]||[]).to be_empty }
  end
end

RSpec.shared_examples "non-empty report" do |scanned_files|
  specify { expect(File.exist?(@report_path)).to be_truthy }

  describe "version" do
    specify { expect(@report["version"]).to_not be_empty }
  end

  describe "scan" do
    specify { expect(@report["scan"]).to_not be_empty }
  end

  describe "vulnerabilities" do
    specify { expect(@report["vulnerabilities"]).to_not be_empty }
  end

  describe "dependency_files" do
    specify { expect(@report["dependency_files"]||[]).to_not be_empty }
  end

  it "matches scanned files" do
    want = scanned_files.sort
    expect(@report["vulnerabilities"].map{|v| v.dig("location", "file")}.uniq.sort).to eql want
    expect(@report["dependency_files"].map{|df| df.dig("path")}.uniq.sort).to eql want
  end
end

RSpec.shared_examples "recorded report" do |expectation|
  let(:want) do
    # return first argument unless this is the expectation name
    unless expectation.is_a? String
      return @expected_report ||= expectation
    end

    # else use expectation name to calculate the path to the expected report
    report_filename = "gl-dependency-scanning-report.json"
    analyzer_dir = ENV.fetch("CI_PROJECT_DIR", ENV.fetch("PWD", "."))
    path = File.join(analyzer_dir, "qa/expect", expectation, report_filename)
    JSON.parse File.read path
  end

  describe "version" do
    specify { expect(@report["version"]).to eql want["version"] }
  end

  describe "scan" do
    def comparable(scan)
      # do not touch the original scan object otherwise schema validation
      # will fail because of removed properties.
      # Marshal functions are used to perform a deep cloning;
      # the built-in clone method only performs a shallow cloning.
      scan = Marshal.load(Marshal.dump(scan))
      scan.delete "start_time"
      scan.delete "end_time"
      scan["scanner"].delete "version" if scan["scanner"]
      scan
    end

    it "is equivalent" do
      expect(comparable(@report["scan"])).to eql comparable(want["scan"])
    end
  end

  describe "dependency_files" do
    it "match expectation" do
      got = @report
      expect(got["dependency_files"].sort).to eql want["dependency_files"].sort
    end
  end

  describe "vulnerabilities" do
    def comparable(vulnerabilities)
      vulnerabilities.map do |v|
        v.delete "id"
        v["links"].sort!{|a, b| a["url"] <=> b["url"]}
        v["identifiers"].sort!{|a, b| a["value"] <=> b["value"]}
        v["location"]["dependency"].delete "iid" if v.dig("location", "dependency")
        v
      end.sort{|a, b| a["cve"] <=> b["cve"]}
    end

    it "match expectation" do
      got = @report
      expect(comparable(got["vulnerabilities"])).to eql comparable(want["vulnerabilities"])
    end
  end
end

RSpec.shared_examples "valid report" do

  it "passes schema validation without errors" do
    # force schema to draft-04 because the security report schemas use draft-07
    # but it's not supported, making JSON::Validator fails with JSON::Schema::SchemaError
    # "Schema not found: http://json-schema.org/draft-07/schema#".
    schema = JSON.parse File.read SchemaResolver.new(@report).path
    schema["$schema"] = "http://json-schema.org/draft-04/schema#"

    # set dependency_files field to an empty array if not set
    # because it's required in Dependency Scanning report schema v3.0.0 and later.
    # See https://gitlab.com/gitlab-org/gitlab/-/issues/300877 for details
    @report["dependency_files"] ||= []

    expect(JSON::Validator.fully_validate(
      schema, @report, parse_data: false, errors_as_objects: true
    )).to eql([])
  end
end
