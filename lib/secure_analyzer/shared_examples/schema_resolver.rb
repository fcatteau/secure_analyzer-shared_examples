class SchemaResolver
  attr_reader :report

  def initialize(report)
    @report = report
  end

  def path
    return File.join root_path, dist_dir, filename
  end

  def dist_dir
    return "v" + report["version"]
  end

  def root_path
    ENV.fetch("SECURITY_REPORT_SCHEMAS_DIST_PATH", "/usr/share/security-report-schemas")
  end

  def filename
    scan_type = report.dig("scan", "type")
    short_name = scan_type.sub("_", "-")
    return "#{short_name}-report-format.json"
  end
end
