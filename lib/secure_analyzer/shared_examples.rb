# frozen_string_literal: true

require_relative "shared_examples/version"
require_relative "shared_examples/report_shared_examples"
require_relative "shared_examples/scan_shared_examples"

module SecureAnalyzer
  module SharedExamples
  end
end
